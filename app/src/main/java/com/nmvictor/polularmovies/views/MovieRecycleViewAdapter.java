package com.nmvictor.polularmovies.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nmvictor.polularmovies.R;
import com.nmvictor.polularmovies.model.Movie;

import java.util.List;

import static com.nmvictor.polularmovies.Constants.MOVIEDB_IMAGE_PREFIX_URL;

/**
 * Recycle view adapter for {@link Movie} objects.
 * Created by nmvictor on 2/14/17.
 */

public class MovieRecycleViewAdapter extends RecyclerView.Adapter<MovieRecycleViewAdapter.ViewHolder> {

    private final Context context;
    private final List<Movie> movies;

    public MovieRecycleViewAdapter(Context context, List<Movie> movieList) {

        this.context = context;
        this.movies = movieList;

    }

    @Override
    public MovieRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false));
    }

    @Override
    public void onBindViewHolder(MovieRecycleViewAdapter.ViewHolder holder, int position) {

        Movie movie = this.movies.get(position);
        holder.mtitleTv.setText(movie.getTitle());
        Glide.with(context)
                .load(MOVIEDB_IMAGE_PREFIX_URL+ movie.getPoster_url())
                .crossFade()  // TODO insert place holder image
            .into(holder.mposterProfleImageView);

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mtitleTv;
        private ImageView mposterProfleImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            mtitleTv = (TextView) itemView.findViewById(R.id.titleTv);
            mposterProfleImageView = (ImageView) itemView.findViewById(R.id.posterIv);

        }
    }
}
