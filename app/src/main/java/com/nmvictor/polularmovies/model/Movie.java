package com.nmvictor.polularmovies.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class to represent a Movie from MovieDB api call.
 * Created by nmvictor on 2/14/17.
 */

public class Movie {
    private String title;
    private String overview;
    @SerializedName("poster_path")
    private String poster_url;

    @SerializedName("vote_average")
    private double rating;

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }

    public String getPoster_url() {
        return poster_url;
    }

    public double getRating() {
        return rating;
    }
}
