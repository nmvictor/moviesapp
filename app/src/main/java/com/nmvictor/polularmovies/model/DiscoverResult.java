package com.nmvictor.polularmovies.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Result from discover movies API call.
 * Created by nmvictor on 2/14/17.
 */

public class DiscoverResult {
    int page;
    @SerializedName("results")
    List<Movie> movies;


    public int getPage() {
        return page;
    }

    public List<Movie> getMovies() {
        return movies;
    }
}
