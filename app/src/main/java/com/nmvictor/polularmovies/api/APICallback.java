package com.nmvictor.polularmovies.api;

import android.support.annotation.Nullable;

/**
 * Created by nmvictor on 2/14/17.
 */

public interface APICallback<T> {

    void onResult(T result);
    void onError(String error,@Nullable Exception e);
}
