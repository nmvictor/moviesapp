package com.nmvictor.polularmovies.api;

import android.support.annotation.NonNull;

import com.nmvictor.polularmovies.model.DiscoverResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.nmvictor.polularmovies.Constants.MOVIEDB_API_BASE_URL;
import static com.nmvictor.polularmovies.Constants.MOVIEDB_API_KEY;

/** Singleton to access api functions
 * Created by nmvictor on 2/14/17.
 */

public class ApiUtil {
    private static ApiUtil instance;
    private Retrofit retrofit;
    private ApiUtil() {
        retrofit = new Retrofit.Builder()
                .baseUrl(MOVIEDB_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public static ApiUtil getInstance() {
        if(instance == null) {
            instance = new ApiUtil();
        }
        return instance;
    }

    public void discoverMovies(final APICallback<DiscoverResult> resultAPICallback) {
        MovieDBApi api = retrofit.create(MovieDBApi.class);
        Call<DiscoverResult> resultCall = api.discoverMovies(MOVIEDB_API_KEY);
        resultCall.enqueue(new Callback<DiscoverResult>() {
            @Override
            public void onResponse(Call<DiscoverResult> call, Response<DiscoverResult> response) {
                if(resultAPICallback != null)
                {
                    if(response.isSuccessful()) {
                        resultAPICallback.onResult(response.body());
                    } else {
                        resultAPICallback.onError("Unknown Error", null);
                    }
                }

            }

            @Override
            public void onFailure(Call<DiscoverResult> call, Throwable t) {
                if(resultAPICallback != null)
                {
                    resultAPICallback.onError(t.getLocalizedMessage(), new Exception(t.getMessage()));
                }

            }
        });
    }
}
