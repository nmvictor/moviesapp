package com.nmvictor.polularmovies.api;

import com.nmvictor.polularmovies.model.DiscoverResult;
import com.nmvictor.polularmovies.model.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nmvictor on 2/14/17.
 */

public interface MovieDBApi  {

    @GET("discover/movie")
    Call<DiscoverResult> discoverMovies(@Query("api_key") String key);

}
