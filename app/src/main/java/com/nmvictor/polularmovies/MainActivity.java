package com.nmvictor.polularmovies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.nmvictor.polularmovies.api.APICallback;
import com.nmvictor.polularmovies.api.ApiUtil;
import com.nmvictor.polularmovies.model.DiscoverResult;
import com.nmvictor.polularmovies.model.Movie;
import com.nmvictor.polularmovies.views.MovieRecycleViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private RecyclerView recyclerView;

    private GridLayoutManager mGridlayoutManager;
    private MovieRecycleViewAdapter adapter;

    private List<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycleView);

        movies = new ArrayList<>();

        setRececleView();
    }


    private void setRececleView() {
        recyclerView.setHasFixedSize(true);

        mGridlayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(mGridlayoutManager);

        adapter = new MovieRecycleViewAdapter(this, this.movies);
        recyclerView.setAdapter(adapter);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(movies.isEmpty()) {
            loadMovies();
        }
    }

    private void loadMovies() {
        ApiUtil.getInstance().discoverMovies(new APICallback<DiscoverResult>() {
            @Override
            public void onResult(DiscoverResult result) {

                if(result != null) {
                    movies.addAll(result.getMovies());
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String error, Exception e) {
                Log.e(TAG, "API Error: "+ error, e);
                showError(error);
            }
        });
    }

    private void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}
