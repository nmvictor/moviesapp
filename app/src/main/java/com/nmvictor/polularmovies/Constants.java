package com.nmvictor.polularmovies;

/**
 * Constants for the project
 * Created by nmvictor on 2/14/17.
 */

public class Constants {

    public static final String MOVIEDB_API_BASE_URL = "https://api.themoviedb.org/3/";
    public static final String MOVIEDB_API_KEY = "9a928f01255bbe816cdaa0c5473b8c62";
    public static final String MOVIEDB_IMAGE_PREFIX_URL = "https://image.tmdb.org/t/p/w185";

}
